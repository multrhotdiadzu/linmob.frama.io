+++
title = "Weekly GNU-like Mobile Linux Update (1/2023): Happy New Year!"
date = "2023-01-08T21:45:00Z"
draft = false
[taxonomies]
tags = ["FOSDEM 2023",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

An interesting blog post by Mobian, and little bits here and there. Oh, and, impressive camera improvements coming to the Librem 5!
<!-- more -->

_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#77 Happy New Year!](https://thisweek.gnome.org/posts/2023/01/twig-77/). _I've tried GTK4 Tangram, and it's great!_
- jrb: [Crosswords: Puzzle update](https://blogs.gnome.org/jrb/2023/01/03/crosswords-puzzle-update/)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: big UI improvements!](https://pointieststick.com/2023/01/06/this-week-in-kde-big-ui-improvements/)
- Volker Krause: [November/December in KDE PIM](https://www.volkerkrause.eu/2023/01/07/kde-pim-november-december-2022.html)
- KDE Announcements: [KDE Gear 22.12.1](https://kde.org/announcements/gear/22.12.1/)
- KDE Announcements: [KDE Plasma 5.26.5, Bugfix Release for January](https://kde.org/announcements/plasma/5/5.26.5/)
- TSDGeos: [The KDE Qt5 Patch Collection has been rebased on top of Qt 5.15.8](https://tsdgeos.blogspot.com/2023/01/the-kde-qt5-patch-collection-has-been.html)
- Carl Schwan: [Tokodon 23.01.0 release](https://carlschwan.eu/2023/01/02/tokodon-23.01.0-release/). _Great news!_

#### Ubuntu Touch
- [Codemaster Freders: "This is how workspaces work on the JingPad on Ubuntu Touch 20.04…"](https://mastodon.social/@fredldotme/109645729829122291)

#### Distributions
- Mobian Blog: [A look back in the mirror... And a glimpse of the future!](https://blog.mobian.org/posts/2023/01/08/2022-review/)

#### Non-Linux
- nfeske: [Mobile user interface, not in the face](https://genodians.org/nfeske/2023-01-05-mobile-user-interface)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-01-06](https://matrix.org/blog/2023/01/06/this-week-in-matrix-2023-01-06)
- Matrix.org: [Matrix Community Year In Review 2022](https://matrix.org/blog/2023/01/03/matrix-community-year-in-review-2022)

### Worth noting
- [Are on T-Mobile USA and use MMS? Update!](https://fosstodon.org/@kop316/109643853166570846)
- [A-wai: "I'm pretty sure we can have @mobian running on RISC-V before Android is ready for that platform ;-)…" - Fosstodon](https://fosstodon.org/@awai/109630719132990125)
- [PINE64: "#PineTab2 running mainline #Linux kernel video out to an external monitor…"](https://fosstodon.org/@PINE64/109644602260546456)
- Thanos made some progress on their Ox64 handheld project:
  - [Video output](https://fosstodon.org/@thanosengine/109648447115659837)
  - [Hardware design](https://fosstodon.org/@thanosengine/109630476174256832)
- FOSDEM 2023 Stand locations [have been announced](https://mastodon.social/@flypig/109647828595865342). _Guess where I'll hang out :)_

### Worth reading
- Mobian Blog: [A look back in the mirror... And a glimpse of the future!](https://blog.mobian.org/posts/2023/01/08/2022-review/).
- adamd's place: [Librem5 Automatic Camera Controls](http://adamd.sdf.org/computers/mini/2023/01/02/L5CameraControlsUpdate.html). _I've played with it, and it is really something._
  - TuxPhones: [Purism Librem 5 receives major camera update, video recording capabilities](https://tuxphones.com/purism-librem-5-linux-phone-auto-camera-millipixels-app-photo-video-recording/)
- WilliamTries: [The Best Wordle Clone for Linux Phones: Warble vs. Burble](https://linuxphoneguides.ovh/20230102-LinuxWordleClones.html)
- nfeske: [Mobile user interface, not in the face](https://genodians.org/nfeske/2023-01-05-mobile-user-interface). _Have a look._
- akselmo: [Pointless tribalism in FOSS](https://www.akselmo.dev/2023/01/06/Pointless-tribalism-in-FOSS.html). _Amen!_
- Purism: [Purism to Participate in CES 2023](https://puri.sm/posts/purism-to-participate-in-ces-2023/). _Yawn!_

### Worth watching
- Novaspirit Tech: [I Used a Linux Tablet for 14 days - Juno Computers](https://www.youtube.com/watch?v=DGYMQnZz914)
- Unboxing Tomorrow: [PinePhone Unboxing: 100 Days Later](https://www.youtube.com/watch?v=ElnrtXdoCjk)
- Lup Yuen Lee: [Our #NuttX Kernel Driver for #PinePhone Touch Panel works OK with #LVGL! 🎉 ... Needs optimising 🤔](https://www.youtube.com/watch?v=xE9U5IQPmlg)
- Steven Nortje: [PhinePhone Pro (Shipped Dec 2022) - crash after system update and then loaded Mobian.](https://www.youtube.com/watch?v=HVgZIaqALRQ)
- benny Malik: [Install Ubuntu Touch on Redmi Note 4/4X Mido Snapdragon](https://www.youtube.com/watch?v=VYLfC7b428k)
- OmegaRed: [Flashing Ubuntu Touch into Samsung Galaxy S3 Neo I9301l](https://www.youtube.com/watch?v=NxsnsSYKof4)
- Mkiol: [Kamkast - camera / screen sharing for Sailfish OS](https://www.youtube.com/watch?v=4suAdzDuG3E)
- Continuum Gaming: [Microsoft Continuum Gaming E345: 3 small, but helpful apps](https://www.youtube.com/watch?v=U-lNq7XzsG4). _Sailfish apps!_
- geb: [Numen voice control phone test](https://peertube.tv/w/uzMMQ5nbmsHMkDGGVcS1ZB)
- jero θηζζβ: [Como instalar manjaro en un PinePhone (How to install manjaro on PinePhone)](https://www.youtube.com/watch?v=grFJtkkmuzM)

### Thanks

Huge thanks again to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one! __If you just stumble on a thing, please put it in there too - all help is appreciated!__

PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)

