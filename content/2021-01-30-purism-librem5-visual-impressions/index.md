+++
title = "Purism Librem 5: Visual impressions"
aliases = ["2021/01/30/purism-librem5-visual-impressions.html"]
author = "Peter"
date = "2021-01-30T17:10:00Z"
layout = "post"
[taxonomies]
tags = ["Purism", "Librem 5", "photos"]
categories = ["impressions"]
authors = ["peter"]
+++

_Just a few photos that show why I don't do much product photography usually._

<!-- more -->

![Front side](librem5-front.jpg)
_Front side or "let's have the most boring photo first"._

![Top side](librem5-top.jpg)
_This the top with the headphone jack._

![Right side](librem5-right.jpg)
_On the right side we find a modest Librem 5 logo and volume rocker and power button._

![Bottom side](librem5-bottom-usb.jpg)
_The bottom has the USB 3.0 Type C port._

![Left side](librem5-left.jpg)
_The left side has the SIM and microSD tray and the killswitches (Cellular, WiFi+BT, Cameras + microphone (from top to bottom); only Wifi is switched on in this picture). If you turn them all off, sensors like compass, accelerometer and proximity sensor get disabled as well._

![Back side](librem5-back-with-cover.jpg)
_The back side is quite boring, but the structured plastic feels good to the hand._

![Back side without cover](librem5-backside.jpg)
_This is what it looks like without the back cover._

![Back side, battery removed](librem5-battery-removed.jpg)
_That battery is a thick beast. The slot that's visible is for a 3FF smart card._

![Back side, with LTE and BT/Wifi visible](librem5-back-modem-wifi.jpg)
_Removing an additional cover makes the the Cellular modem (a Broad Mobi BM818-E1 on a m.2 3042 card in my case) and the Redpine Signals RS9116 m.2 2230 card (over SDIO and I<sup>2</sup>C), that handles 802.11 a/b/g/n (WiFi) and BT, visible._

![Back side, with LTE and BT/Wifi visible compared to PinePhone](librem5-pinephone-modem-sizes.jpg)
_Bonus:_ Size comparison to the PinePhone modem. _It sure looks like the Quectel EG25-G modem of the PinePhone could fit on a m.2 3042 card and into the Librem 5._

![Librem5 connected to the HP Elite X3 Lap Dock](librem5-convergence.jpg)
_Bonus 2: The USB-C cable that comes with the Librem 5 is full-featured, so you can connect displays with it, like I did here with my HP Elite X3 Lap Dock. Also in the picture is the 18W USB-PD Librem 5 power supply._

If you want to know what I my initial impressions of the Librem 5 are, head over to the [relevant blog post](https://linmob.net/2021/01/28/purism-librem5-first-impressions.html).
