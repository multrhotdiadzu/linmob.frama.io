+++
title = "It's there"
aliases = ["2008/01/it-s-there.html"]
date = "2008-01-16T02:35:00Z"
layout = "post"
[taxonomies]
tags = ["E28", "E2831", "neuf", "platforms"]
categories = ["impressions", "hardware"]
authors = ["peter"]
[extra]
2021_commentary = "I would not call myself a hacker now, but these Linux Phones have set me on a journey that would enable me to proceed way faster today than I did back then."
+++
The E2831 arrived today morning.
<!-- more -->

It's a really thin and light phone, nice to hold in your hand, fingerprinty.
The hardware's OK, the software&#8230; well, it is nice software, somehow, but it'll be really difficult to install additional software to start in GUI.
But there is something, that is even worse: I'm unable to telnet it (up to now at least) - and because of this I'm unable to unlock it.

I believe that an alternative software stack for this device should be relatively easy to create, as QTopia supports the OMAP730 which powers this phone - and the kernel support should be good as well. Only problem: I'm unable to do it, as I am no hacker.

Unfortunately.
