+++
title = "Android 2.3.3 on the Samsung GT-i8320"
aliases = ["2011/05/10/android-2-3-3-on-the-samsung-gt-i8320.html"]
author = "peter"
date = "2011-05-10T15:40:00Z"
layout = "post"
[taxonomies]
categories = ["shortform", "software",]
tags = ["Android 2.3 Gingerbread", "Android ports", "h1droid", "LiMo", "Samsung", "Samsung GT-i8320"]
authors = ["peter"]
+++
<p>It's not a huge step and I haven`t tried this out myself yet, but I am very happy to see some Gingerbread coming to the <b>Samsung GT-i8320</b> (you know, that's this mid-2009 phone with WVGA AMOLED screen, TI OMAP 3430 which ran a LiMo compliant OS OOTB) - as there is little ..err.. no evidence that anyone is porting over 2.3.x to the Acer Stream it is nice to have one device that runs the most beautiful iteration of Android for smartphones yet at faster than G1 speeds.

Don't expect this to be perfectly reliable yet, there are a few things that simply don't work yet:
</p><ul><li>GPRS / 3G data</li><li> Cameras </li><li>GPS</li><li>SMS are said to be unreliable</li><li>apparently there are (almost random?) restarts</li></ul>Still, it's great to see this project improving. Kudos to everyone involved, especially to <i>mephisto</i>, the chinese mastermind behind this port. 

<a href="http://code.google.com/p/h1droid/">h1droid Project page (Google Code)</a>
<a href="http://samsungi8320.freeforums.org/index.php">Samsung i8320 Forums</a>

