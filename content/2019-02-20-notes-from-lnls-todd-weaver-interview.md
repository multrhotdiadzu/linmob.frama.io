+++
title = "Notes from LNLs Todd Weaver Interview"
aliases = ["2019/02/20/notes-from-lnls-todd-weaver-interview.html"]
author = "Peter"
date = "2019-02-20T08:12:53Z"
layout = "post"
[taxonomies]
tags = ["developer device", "hardware", "i.MX8M", "Librem 5", "linux", "linux on mobile devices", "PureOS", "Purism", "smartphones", "transcript"]
categories = ["hardware","commentary"]
authors = ["peter"]
+++
<p>On <a href="https://latenightlinux.com/late-night-linux-episode-57/">Episode 57 (published Feb. 19th, 2019) of the Late Night Linux</a> podcast Purism CEO Todd Weaver was interviewed by podcast host Joe Ressington. Below is a brief summary in bullet points:</p>
<!-- more -->

<ul>
<li><em>Generally</em> Purism is doing "extremely well": Year over year triple digit growth rates, overall community support and achievements are great.</li>
<li><strong>Laptops:</strong> Coreboot, neutered Management Engine, security story is great
<ul>
<li>Soon Pureboot (Coreboot + Heads + TPM + Librem Key) will be announced: Tamper evident systems.</li>
</ul>
</li>
<li><strong>Librem 5 hardware</strong>
<ul>
<li>"Shipping hardware is hard"</li>
<li>CPU issue ended up moving the Phone to Q2, mayor update soon</li>
<li>after development kit shipments interest went up, flood of orders </li>
<li>dev kit problems: Screen not working, neither does HDMI out </li>
<li><em>point of development kit:</em> get developers work on hardware close to actual hardware, phosh speed, core applications need to be improved and will be, "one time programming" necessary to get screens to work, as SoC can't send initialization code to the screen (NXP i.MX8M buffer to small, silicon bug?). Every developer kit screen will be enabled by software work with NXP or sending out One Time Programming kits.</li>
<li><em>"dev kit size enormous"</em>: Dev Kit is a break out board for the SoM which has all the most complex parts of the system, SoM is small and right for phone size. Phone is going to be about 14mm thick (or so), similar to kind of iPhone 4 original thickness</li>
<li><em>Massive heat sink on dev kit, fan on postmarketOS photo, how will this be cooled in the phone?</em> Errata against CPU; no power management early on; initial temperature 90°, now by software improvements down to 34°, more optimizations coming like idle state</li>
<li><em>iMX8 not designed to be in a phone. Snapdragon would be clearly designed to be in a phone. iMX8 has a pretty high power draw. is more for mains connected devices. </em> True. But: There are no mobile chips that offer "complete freedoms". i.MX6 and i.MX8M are helping with freedoms, i.MX8M will be dropping to 14nm in 2019, so power consumption will improve with a later hardware revision. Good roadmap. i.MX8M vs. i.MX8Mini: GPU differences, …</li>
<li><em>Target for idle battery life:</em> One work day battery life. Confident it will be reachable for phone ship date.</li>
<li><em>When will the phone ship? </em>Q2 or Q3 (1th of April to 30th of September), everything is marching forward, big problem was the silicon bug, created delay in fabrication, but software stack development progressed very nicely.</li>
</ul>
</li>
<li><strong>Librem 5 software</strong>
<ul>
<li><em>Software store: </em>Going remarkably well. Easy way for people to recognize what applications are available and does it respect me as an individual. </li>
<li><em>How many apps are going to be phone optimized at launch?</em> Campaign promised 5 apps for typical tasks: Phone call, Browser, e mail, messaging. But: Libhandy is in GTK proper, porting an application over just means changing a few classes. Music player, settings application, contacts incoming. Even some game developers started development. People want to be part of the ecosystem.</li>
<li><em>In house development of apps vs. community efforts: </em>Focus on initial 5 internally (80 % Purism, 20 % community), Fractal (Matrix client) funded development, some applications are going to be entirely written by the community. </li>
<li><em>Community ports:</em> Some working, ..., as dev kits got shipped to early backers first and then to partnerships. Plasma Mobile is advancing, UBports will receive dev kits soon.</li>
<li><em>Android:</em> No interest in Android backup plan, fine with people working on it. The mission for Purism is to solve the long term problem of having a phone that respects people.</li>
</ul>
</li>
<li><strong>Twitter questions: </strong>
<ul>
<li><em>App store: </em>Elementary OS-style "pay what you want" thing? Yes, talked to them. Want to have a curated set of applications with options of donating to the developer, a pay the developer process, a subscription process or straight up gratis. Working on it, is going to be part of the Purism store.</li>
<li><em>Number of developers</em> working on Pure OS mobile: Close to 20.</li>
<li><em>Will Signal be supported? </em>Community folks are working on the APIs to have Signal work. Purism have connections with signal, of all the applications out there it is the most likely one to be included by default and meet the criteria Purism have.</li>
<li><em>Different Mobile hardware? </em>It's going to be i.MX8 going forward, next silicon version will lead to 2nd gen Librem 5. Qualcomm or Mediatek are not on the table for the near future.</li>
<li><em>Other devices: </em>Lot's of other things on the table. Services coming, version 5 of Laptops.</li>
<li><em>Ethical subscription services:</em> Purism will be launching a bundle of services based on decentralized services under on simple account. Free and paid tier. VPN, E-mail, chat, video and voice calling, social media, all in one. Virtual phone numbers, cloud storage. Convenient, but also completely respecting user freedoms. Will launch before the phone, services have been used before internally, will be scaled up for external users. Cross plattform, Android, iOS, desktop platforms. Federated, so people can run their own.</li>
<li><em>MIPS or RISC-V?</em> Testing RISC-V, following very closely. Within a couple of years Purism will have some kind of a RISC-V product, maybe a router, as the platform moves along.</li>
<li><em>Still working without VC money?</em> Yes. Have completely avoided VCs and will continue to do so.</li>
</ul>
</li>
</ul>

